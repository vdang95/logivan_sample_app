# README

This README would normally document whatever steps are necessary to get the
application up and running.

This application is a simple shopping application. User who is a member of this website will be able to view and purchase products of different types. Admin of this website can view and modify the information of each products, as well as creating and changing the promotion type of each product. 

 Database creation: 
 Please run the following scripts to initialize the database:
 2. To create the first five new product bases
 ActiveRecord::Base.transaction do
   for i in 1..5 do
     new_name = "Item " + i.to_s
     new_product_code = i
     new_price = i*10
     new_quantity_in_store = i*2
     p = ProductBase.new(name: new_name, quantity_in_store: new_quantity_in_store, product_code: new_product_code, price: new_price) 
     p.save
   end
 end

 3. To create the first five new users
 ActiveRecord::Base.transaction do
   for i in 1..5 do
     new_name = "User name " +i.to_s
     new_password = "Password " + i.to_s
     new_password_confirmation = "Password " + i.to_s
     user = User.new(name: new_name, password: new_password, password_confirmation: new_password_confirmation)
     user.save
   end
 end

 4. To make the first user admin, so that he/she can add or modify the promotion rule
 User.find(1).add_role("Admin")
