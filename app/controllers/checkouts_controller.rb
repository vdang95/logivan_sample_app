class CheckoutsController < ApplicationController
  before_action :set_checkout , only: [:show, :edit, :destroy, :update]
  before_action :set_user, only: [:show,:create,:new, :edit, :update]

  def show 
  end

  def new
    @checkout = Checkout.new
  end

  def edit
  end

  def create
    @checkout = Checkout.new(checkout_params)
    @checkout.add_product_base(checkout_params[:product_base_id])
    respond_to do |format|
      if @checkout.save
        format.html { redirect_to user_manage_path(@user), notice: 'New promotion rule was successfully created.' }
        format.json { render :show, status: :created, location: @checkout }
      else
        format.html { render :new, json: @checkout.errors }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update 
    @checkout = Checkout.find(params[:id]) 
    respond_to do |format|
      if (@checkout.update(checkout_params))
        format.html { redirect_to user_manage_path(@user), notice: "Successfully updated!"}
        format.json { render :show, status: :ok, location: user_manage_path(@user)}
      else
        format.html { render :edit, notice: "not working bro"}
        format.json { render json: @checkout.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
  def set_checkout 
    @checkout = Checkout.find(params[:id])
  end

  def set_user
    @user = User.find(params[:user_id])
  end


  def checkout_params
    params.require(:checkout).permit(:name,:promotion_type, :quantity, :new_price, :new_percentage, :product_base_id)
  end

end
