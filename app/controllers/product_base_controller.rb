class ProductBasesController < ApplicationController
  def index
    @product_bases = ProductBase.all 
  end  

  def show
  end

  def destroy
    @product_base.destroy
  end

  def new
  end

  def edit
  end

  def update
  end

  private
  def set_product_base
    @product_base = ProductBase.find(params[:id])
  end

  def product_base_params 
    params.require(:product_base).permit(:product_code, :name, :price, :quantity_in_store)
  end

end
