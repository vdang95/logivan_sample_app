class ProductBasesController < ApplicationController
  before_action :set_product_base, only:[:show, :edit, :update, :destroy]
  before_action :set_user, :user_has_role_admin?, only: [:new, :create,:edit,:show]

  def index
    @from_home_page = true 
    if (!params[:user_id].nil?) 
      @user = User.find(params[:user_id])
      @from_home_page = false 
      @user_has_role_admin = @user.has_role?("Admin")
    end
    @product_bases = ProductBase.all 
  end  

  def show
    @from_home_page = true 
    if (!params[:user_id].nil?)
      @user = User.find(params[:user_id])
      @from_home_page = false
    end
  end

  def manage
    @product_bases = ProductBase.all
    @user = User.find(params[:user_id])
  end

  def destroy
    @product_base.destroy
  end

  def new
    @product_base = ProductBase.new
  end

  def edit
    @product_base = ProductBase.new
  end

  def create
    @product_base = ProductBase.new(product_base_params)
    respond_to do |format|
      if @product_base.save
        format.html { redirect_to user_manage_path(@user), notice: 'Product base was successfully created.' }
        format.json { render :show, status: :created, location: user_manage_path }
      else
        format.html { render :new, json: @product_base.errors }
        format.json { render json: @product_base.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if (@product_base.update(product_base_params))
        format.html { redirect_to @product_base, notice: "Successfully updated!"}
        format.json { render :show, status: :ok, location: @product_base}
      else
        format.html { render :edit, notice: "not working bro"}
        format.json { render json: @product_base.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def set_product_base
    @product_base = ProductBase.find(params[:id])
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def user_has_role_admin?
    @user_has_role_admin = User.find(params[:user_id]).has_role?("Admin")
  end

  def product_base_params 
    params.require(:product_base).permit(:product_code, :name, :price, :quantity_in_store)
  end
end
