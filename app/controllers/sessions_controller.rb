class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email:params[:session][:email].downcase)
    if (user && user.authenticate(params[:session][:password]))
      # Redirect the user to the show page
      log_in user
      redirect_to user
    else
      # Display an error message of email/password is invalid 
      flash[:dander] = "Invalid email/password login"
      render 'new'
    end
  end

  def destroy
  end
end
