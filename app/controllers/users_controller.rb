class UsersController < ApplicationController
  before_action  :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    @user_has_role_admin = @user.has_role?("Admin")
  end

  def update_basket 
    @user = User.find(params[:user_id])
    @product_quantity = params[:product_quantity].to_i
    @product_code = params[:product_code].to_i
    respond_to do |format|
      if @user.add_to_basket(@product_code, @product_quantity)
        format.html{ redirect_to user_product_bases_path(@user), notice: "You have added the products!" }
        format.json{ head :no_content}
      else
        format.html{ redirect_to user_product_bases_path(@user), notice: "You have added the products!" }
        format.json{ head :no_content}
      end
    end
  end

  def checkout
    @user ||= User.find(params[:user_id])
    @products = @user.products
    @total_price = @user.get_total_prices
  end

  def show_products 
    @user = User.find(params[:user_id])
    @redirect_from_product_bases = params[:from_product_bases]
    @products = User.find(params[:user_id]).products 
  end

  def list_products 
    @user = User.find(params[:user_id])
    @products = User.find(params[:user_id]).products
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        @user.add_role("Individual")
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, json: @user.password }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update 
    # You are not allowed to update your passwords in the edit 
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password_confirmation].blank?
    respond_to do |format|
      if @user.update_attribute(:name, params[:user][:name]) && @user.update_attribute(:email, params[:user][:email])
        format.html { redirect_to @user, notice: "Successfully updated!"}
        format.json { render :show, status: :ok, location: @user}
      else
        format.html { render :edit, notice: "not working bro"}
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:product_code, :product_quantity, :name, :email, :password, :password_confirmation)
    end

    def find_routes 
      @current_uri = request.env["PATH_INFO"]
      path = Rails.application.routes.recognize_path(@current_uri)
      @controller_name = path[:controller]
      @action_name = path[:action] 
    end
  end