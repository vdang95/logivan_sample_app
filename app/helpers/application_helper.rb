module ApplicationHelper
  # Return the full title of the whole page
  def full_title(page_title = '')
    base_title = "Logivan Application"
    if (page_title.empty?)
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
