class Checkout < ApplicationRecord
  validates :name, presence: :true
  validates :promotion_type, presence: :true
  validates :quantity, presence: :true
  has_many :product_bases, class_name: "ProductBase"

  def scan(item)
    if (self.promotion_type == "priceBased")
      if (item.price * item.quantity > self.quantity)
        return (item.price * new_percentage)/100
      end
    elsif (self.promotion_type == "quantityBased")
      if (item.quantity > self.quantity)
        return self.new_price 
      end
    end
    return item.price
  end

  def add_product_base(product_base_id)
    product_base = ProductBase.find(product_base_id)
    self.product_bases << product_base
  end
end
