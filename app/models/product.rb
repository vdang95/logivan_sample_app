class Product < ApplicationRecord
  belongs_to :product_base
  belongs_to :user

  validates_inclusion_of :price, in: 0..1000000000 
  validates_inclusion_of :quantity, in: 0..100000

  def show_users_products 
    return self.product_code  + self.quantity + self.name
  end

  def get_price 
    if (self.product_base.checkout.nil?)
      return self.price
    else
      return self.product_base.checkout.scan(self)
    end
  end
end
