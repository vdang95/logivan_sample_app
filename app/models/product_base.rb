class ProductBase < ApplicationRecord
  has_many :products
  belongs_to :checkout , required: false 
end
