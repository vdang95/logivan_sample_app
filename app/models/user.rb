class User < ApplicationRecord
  has_and_belongs_to_many :roles
  has_many :products 

  before_save {self.email = email.downcase}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :password, presence: true, :if => :password
  validates :password_confirmation, presence: true, :if => :password_confirmation
  validates_length_of(:password, minimum: 6)
  validates_length_of(:password_confirmation, minimum: 6)

  validates(:name, presence: true)
  validates(:email, presence: true)
  validates_length_of(:name, maximum: 100)
  validates_length_of(:email, maximum: 100)

  validates(:email,{format: {with: VALID_EMAIL_REGEX}, uniqueness: true})

  has_secure_password

  def has_role?(role_name)
    if (self.roles.find_by(name: role_name).nil?)
      return false 
    end
    true 
  end

  def add_role(role_name)
    self.roles << Role.find_by(name: role_name)
    self.save
  end

  def get_total_prices
    @total_price = 0
    self.products.each do |product|
      item_checkout = product.product_base.checkout
      if (!item_checkout.nil?)
        new_price = item_checkout.scan(product) 
        @total_price = @total_price + product.quantity * new_price 
      else
        @total_price = @total_price + product.quantity * product.price 
      end
    end 
    @total_price
  end

  def add_to_basket(product_code, product_quantity)
    product_base = ProductBase.find_by(product_code: product_code)
    remaining_quantity = product_base.quantity_in_store - product_quantity
    # Checking if the product already exists in the user's basket
    product = self.products.find_by(product_code: product_code)
    if (product.nil?)
      new_product = product_base.products.create(user_id: self.id, quantity: product_quantity, :product_code => product_code, :name => product_base.name, :price => product_base.price)
    else
      product.update_attribute(:quantity, product.quantity+product_quantity )
    end
    product_base.update_attribute(:quantity_in_store, remaining_quantity)
  end
end