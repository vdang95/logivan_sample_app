Rails.application.routes.draw do
  root "homes#index"
  get 'sessions/new'
  get "homes/about"
  get "homes/help"

  resources :homes
  resources :products

  resources :users do
    get "checkout" => "users#checkout"
    get "products" => "users#show_products"
    get "products/:id" => "products#show"
    resources :product_bases
    get "update_basket" => "users#update_basket"
    get "manage" => "product_bases#manage"
    get "/manage/checkouts/:product_base_id/new" => "checkouts#new"
    post "/manage/checkouts" => "checkouts#create" 
    get "/manage/checkouts/:id" => "checkouts#show"
    get "/manage/checkouts/:id/edit" => "checkouts#edit", as: :edit_checkout
    patch "/manage/checkouts/:id/update" => "checkouts#update", as: :update_checkout
  end

  resources :product_bases

  get "/login" => "sessions#new"
  post "/login" => "sessions#create"
  delete "/logout" => "sessions#destroy"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
