class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :product_code
      t.string :double
      t.string :name
      t.string :text
      t.string :price
      t.string :double

      t.timestamps
    end
  end
end
