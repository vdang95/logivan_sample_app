class AddUserIdToProducts < ActiveRecord::Migration[5.2]
  def up
    add_column :products, :user_id, :integer
  end

  def down
    remove_column :user_id, :products
  end
end
