class ProductsUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :product_users do |t|
      t.integer :product_id
      t.integer :user_id
    end
  end
end
