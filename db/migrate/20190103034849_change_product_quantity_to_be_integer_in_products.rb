class ChangeProductQuantityToBeIntegerInProducts < ActiveRecord::Migration[5.2]
  def change
    change_column :products, :quantity, :integer
  end
end
