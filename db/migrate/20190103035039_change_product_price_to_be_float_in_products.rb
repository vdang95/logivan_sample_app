class ChangeProductPriceToBeFloatInProducts < ActiveRecord::Migration[5.2]
  def change
    change_column :products, :quantity, :float
  end
end
