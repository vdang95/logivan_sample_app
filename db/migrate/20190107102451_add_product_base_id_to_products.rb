class AddProductBaseIdToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :product_base_id, :integer
  end
end
