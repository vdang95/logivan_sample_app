class RemoveIndexUserId < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :user_id, name: "index_products_on_user_id"
  end
end
