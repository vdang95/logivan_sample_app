class AddProductBaseIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :product_base_id, :integer
  end
end
