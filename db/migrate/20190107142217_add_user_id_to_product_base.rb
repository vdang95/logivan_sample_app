class AddUserIdToProductBase < ActiveRecord::Migration[5.2]
  def up 
    add_column :product_bases, :user_id, :integer
  end

  def down
    remove_column :user_id, :product_bases
  end
end
