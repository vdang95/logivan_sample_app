class AddIndexProductOnUserId < ActiveRecord::Migration[5.2]
  def up 
    add_index :products, :user_id
  end

  def down
    remove_index :user_id, :products
  end
end
