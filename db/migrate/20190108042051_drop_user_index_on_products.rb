class DropUserIndexOnProducts < ActiveRecord::Migration[5.2]
  def up 
    remove_column :users, :product_id 
    remove_column :products, :user_id
  end

  def down
    add_column :users, :product_id
    add_column :products, :user_id
  end
end
