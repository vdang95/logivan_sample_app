class AddProductReferencesToUser < ActiveRecord::Migration[5.2]
  def up 
    add_reference :users, :product, foreign_key: true
  end

  def down 
    add_reference :products, :user, foreign_key: true
  end
end
