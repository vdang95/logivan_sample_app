class RemoveUserIdOnProductBases < ActiveRecord::Migration[5.2]
  def up 
    remove_column :product_bases, :user_id
  end

  def down
    add_column :product_bases, :user_id
  end
end
