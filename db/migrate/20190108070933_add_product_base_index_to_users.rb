class AddProductBaseIndexToUsers < ActiveRecord::Migration[5.2]
  def up
    add_index :users, :product_base_id
  end

  def down
    remove_index :users, :product_base_id
  end
end
