class AddUserReferencesToProductBases < ActiveRecord::Migration[5.2]
  def up 
    remove_column :product_bases, :user_id
    add_reference :product_bases, :user, foreign_key: true
  end
  
  def down
    remove_reference :product_bases, :user
  end
end
