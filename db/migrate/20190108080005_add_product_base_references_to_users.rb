class AddProductBaseReferencesToUsers < ActiveRecord::Migration[5.2]
  def up 
    remove_column :users, :product_base_id
    add_reference :users, :product_base, index: true 
  end
  
  def down
    remove_reference :users, :product_base
  end
end
