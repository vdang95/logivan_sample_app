class AddProductCodeToProductBases < ActiveRecord::Migration[5.2]
  def change
    add_column :product_bases, :product_code, :string
  end
end
