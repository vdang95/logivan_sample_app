class ChangeProductCodeToBeInteger < ActiveRecord::Migration[5.2]
  def up 
    change_column :products, :product_code, :integer
    change_column :product_bases, :product_code, :integer
  end

  def down
    change_column :products, :product_code, :string
    change_column :product_bases, :product_code, :string
  end
end
