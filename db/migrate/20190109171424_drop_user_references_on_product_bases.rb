class DropUserReferencesOnProductBases < ActiveRecord::Migration[5.2]
  def up
    remove_column :product_bases, :user_id, index:true
  end

  def down
    add_reference :product_bases, :user
  end
end
