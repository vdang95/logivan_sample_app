class CreateCheckouts < ActiveRecord::Migration[5.2]
  def up 
    create_table :checkouts do |t|
      t.string :name
      t.string :type
      t.integer :quantity
      t.timestamps
    end
  end

  def down
    drop_table :checkouts
  end
end
