class AddAttributesToCheckouts < ActiveRecord::Migration[5.2]
  def up
    add_column :checkouts,:name, :string
    add_column :checkouts, :type, :string
    add_column :checkouts, :quantity, :integer
  end

  def down
    remove_column :checkouts,:name, :string
    remove_column :checkouts, :type, :string
    renove_column :checkouts, :quantity, :integer
  end
end
