class AddNewPriceToCheckouts < ActiveRecord::Migration[5.2]
  def change
    add_column :checkouts, :new_price, :float
  end
end
