class AddProductBasesReferencesToCheckouts < ActiveRecord::Migration[5.2]

  def up 
    add_reference :checkouts, :product_base
  end

  def down
    remove_reference :checkouts, :product_base
  end
end
