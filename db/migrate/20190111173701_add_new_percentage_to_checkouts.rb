class AddNewPercentageToCheckouts < ActiveRecord::Migration[5.2]
  def up 
    add_column :checkouts, :new_percentage, :float
  end

  def down
    remove_column :checkouts, :new_percentage, :float
  end
end
