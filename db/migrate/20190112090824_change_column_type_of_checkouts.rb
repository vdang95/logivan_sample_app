class ChangeColumnTypeOfCheckouts < ActiveRecord::Migration[5.2]
  def up 
    rename_column :checkouts, :type, :promotion_type
  end

  def down
    rename_column :checkouts, :type, :promotion_type
  end
end
