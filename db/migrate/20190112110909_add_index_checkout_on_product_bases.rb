class AddIndexCheckoutOnProductBases < ActiveRecord::Migration[5.2]
  def up
    add_index :product_bases, :checkout
  end

  def down
    remove_index :product_bases, name: "index_product_bases_on_checkout"
  end

end
