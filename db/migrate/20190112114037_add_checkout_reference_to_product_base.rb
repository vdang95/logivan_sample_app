class AddCheckoutReferenceToProductBase < ActiveRecord::Migration[5.2]
  def up
    add_reference :product_bases, :checkout  
  end

  def down
    remove_reference :product_bases, :checkout
  end
end
