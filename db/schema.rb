# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_12_114037) do

  create_table "checkouts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "promotion_type"
    t.integer "quantity"
    t.float "new_price"
    t.integer "product_base_id"
    t.float "new_percentage"
    t.index ["product_base_id"], name: "index_checkouts_on_product_base_id"
  end

  create_table "homes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_bases", force: :cascade do |t|
    t.string "name"
    t.integer "quantity_in_store"
    t.integer "price"
    t.integer "product_code"
    t.integer "checkout_id"
    t.index ["checkout_id"], name: "index_product_bases_on_checkout_id"
  end

  create_table "products", force: :cascade do |t|
    t.integer "product_code"
    t.string "name"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.integer "product_base_id"
    t.integer "user_id"
    t.index ["user_id"], name: "index_products_on_user_id"
  end

  create_table "products_users", force: :cascade do |t|
    t.integer "product_id"
    t.integer "user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
  end

  create_table "roles_users", force: :cascade do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.integer "product_id"
    t.integer "product_base_id"
    t.index ["product_base_id"], name: "index_users_on_product_base_id"
    t.index ["product_id"], name: "index_users_on_product_id"
  end

  create_table "welcomes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
