require "rspec"
require './spec/spec_helper.rb'
require './spec/rails_helper.rb'

RSpec.describe ProductsController, :type => :controller do
  before do
    @product = Product.first
    @params = {:id => @product.id,
               :product_code => @product.product_code,
               :name => @product.name,
               :price => @product.price , 
               :quantity => @product.quantity}
  end

  describe 'test connections' do
    it 'should get index' do
      get :index
      expect(response).to(have_http_status(:ok))
    end

    it 'should get show' do
      get(:show, :params => @params)
      expect(response).to have_http_status(:ok)
    end

    it 'should get create' do
      product_params = FactoryGirl.attributes_for(:product)
      expect{post :create, params:{:product => product_params}}.to change(Product, :count).by(1)
    end

    it 'should get edit' do
      get(:edit, :params => @params)
      expect(response).to(have_http_status(:ok))
    end
  end
end
