require './spec/spec_helper'
require './spec/rails_helper'
require 'rspec'

RSpec.describe UsersController, :type => :controller do

  def build_test_user
    @user = FactoryGirl.build(:user)
  end   

  def new_attributes
    {
      name: "Dang Huy Viet",
      email: "123456789@gmail.com",
      password: "newpassword",
      password_confirmation: "newpassword"
    }
  end

  describe "Testing connection" do
    it "should get index " do
      get :index
      expect(response).to have_http_status(:ok)      
    end

    it "should get create" do
      user_params = FactoryGirl.attributes_for(:user)
      expect{post :create, params: {user: user_params}}.to change(User,:count).by(1) 
    end

    it "should get list products" do
      get :list_products
      expect(response).to have_http_status(:ok)
    end

    it "should be edited" do
      # Saving the last attributes of the user
      get :edit, {:params => new_attributes}
      expect(response).to have_http_status(:ok)
    end

    it "should be updated" do
      build_test_user 
      new_attributes = @user.attributes 
      put :update, {:params => new_attributes}
      expect(response).to have_http_status(:ok)
      expect(@user.attributes).to have_attributes(new_attributes)
    end

    it "should get show" do
      get :show, {params: User.first.attributes}
      expect(response).to have_http_status(:ok)
    end

    it "should be destroyed " do
      user_params = FactoryGirl.attributes_for(:user)
      new_user = User.new(user_params)
      new_user.save
      expect{delete :destroy, params: {:id => new_user[:id]}}.to(change(User,:count).by(-1))
    end

    it "should get login" do
      get :login, {params: User.first.attributes}
      expect(response).to have_http_status(:ok)
    end

    it "should be signup" do
    end
  end
end