FactoryGirl.define do
  factory :product do
    product_code "001"
    name "First Product"
    price "002" 
    quantity '3'
  end
end