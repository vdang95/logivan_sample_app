require "rspec"
require "./spec/rails_helper"
require "./spec/spec_helper"

RSpec.describe Product, :type => :model do
  describe "Testing type of each attribute" do

    def build_new_product
      @product = FactoryGirl.build(:product)
    end

    it "product code should be of type String" do
      build_new_product
      expect(@product.product_code).to be_kind_of(String)       
    end

    it "product name should be of type String" do
      build_new_product
      expect(@product.name).to be_kind_of(String)
    end

    it "product price should be of type Float" do
      build_new_product
      expect(@product.price).to be_kind_of(Float)
    end

    it "product quantity should be of type integer" do
      build_new_product
      expect(@product.quantity).to be_kind_of(Integer)
    end
  end

  describe "Testing value limit of each attribute" do

    def build_new_product
      @product = FactoryGirl.build(:product)
    end

    it "product code length should not exceed a hundred" do
      build_new_product
      new_code = "a" * 101
      @product.product_code = new_code 
      expect(@product.save).to eql(false)
    end

    it "product price should not exceed one billion" do
      build_new_product
      new_price = 1000000001 
      @product.price = new_price
      expect(@product.save).to eql(false)
    end

    it "product quantity should not exceed a hundred thousand" do
      build_new_product
      new_quantity = 100001 
      @product.quantity = new_quantity
      expect(@product.save).to eql(false)
    end

    it "product price should be present" do
    end
  
    it "product quantity should be present" do
    end

    it "product code should be present" do
    end

    it "product name should be present" do
    end

  end
end
