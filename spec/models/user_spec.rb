require "./spec/rails_helper"
require "./spec/spec_helper"
require "rspec"

RSpec.describe User, :type => :model do

  def build_new_user 
    @user = FactoryGirl.build(:user)
  end

  describe "Testing basic functionalities" do

    it "email should not be blank" do
      build_new_user
      new_email = " "* 6
      @user.email = new_email
      expect(@user).to be_invalid 
    end
    
    it "name should not be blank" do
      build_new_user
      new_name = " " * 6
      @user.name = new_name
      expect(@user).to be_invalid
    end

    it "password should not be blank" do
      build_new_user
      new_password = " "*6
      new_password_confirmation = " " * 6
      @user.password = new_password
      @user.password_confirmation = new_password_confirmation 
      expect(@user.save).to eql(false) 
    end

    it "password should match password confirmation" do
      build_new_user
      new_password = "abcdefg"
      new_password_confirmation = "abcdefghijk"
      @user.password = new_password
      @user.password_confirmation = new_password_confirmation 
      expect(@user.save).to eql(false) 
    end

    it "name should be long enough" do
      build_new_user
      new_name = "a" * 100
      @user.name = new_name
      expect(@user.save).to eql(true)
    end

    it "email should be of valid format" do
      build_new_user
      test_email_list = ["vdang@gustavus.edu", "vietdanghuy.95@gmail.com", "danghuyviet@gmail.com"]
      test_email_list.each do |em|
        @user.email = em
        expect(@user.save).to eql(true)
      end
    end

    it "email should be of invalid format" do
      build_new_user
      test_email_list = ["abcd", "abcdef", "abcdefgh", "abcdefghijk"]
      test_email_list.each do |em|
        @user.email = em
        expect(@user.save).to eql(false) 
      end
    end

    it "name should be long enough" do
      build_new_user
      new_name = "a" * 100 
      @user.name = new_name
      expect(@user.valid?).to eql(true)
    end

    it "name should be too long" do
      build_new_user
      new_name = "a" * 101 
      @user.name = new_name
      expect(@user.valid?).to eql(false)
    end

    it "email should be long enough" do
      build_new_user
      new_name = "a" * 90 + "@gmail.com" 
      @user.name = new_name
      expect(@user.valid?).to eql(true)
    end

    it "email should be too long" do
      build_new_user
      new_email = "a" * 91 + "@gmail.com"
      @user.email = new_email
      expect(@user.valid?).to eql(false)
    end

    it "email should be lower case before save" do
      build_new_user
      test_email_list = ["VietDanghuy.956@gmail.com", "VIETDANGHUY.956@gmail.com", "VietDangHuy.956@GMAIL.COM"]
      test_email_list.each do |em|
        @user.email = em
        expect(@user.save).to eql(true)        
        expect(@user.email).to eql("vietdanghuy.956@gmail.com")
        # Change all the emails back to before being modified
        @user.email = "vietdanghuy.95@gmail.com"
        @user.save
      end
    end

  end

  describe "Testing the more complicated functionalities" do
    it "this should return the total price of all the products in the basket" do
      build_new_user 
      EXPECTED_PRICE = 30 
      # Create a bunch of mock products here 
      (1..5).each do |x|
        new_product = Product.new(:price => x, :quantity => 2)
        new_product.save
        @user.products << new_product
      end
      # Add the total price of all the products into one
      expect(@user.get_total_prices.to_i).to eql(EXPECTED_PRICE)
    end

    it "should add to the basket respectively the product_id and product_quantity" do
      build_new_user
      product_id = 1
      product_quantity = 2
      @user.update_basket(product_id,product_quantity)
      @user.save
      product 
    end
  end

end