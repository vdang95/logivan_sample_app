require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = User.first
  end

  test "should create user" do
    new_email = "adkjfasldfkj@gmail.com"
    new_name = "laskjdflakjsdflkjasdfjlasdkjflskjf"
    assert_difference('User.count') do
      post users_url, params: {user:{email: @user.new_email, name: @user.new_name}}
    end
    assert_redirected_to user_url(User.last)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should get the show_products page" do
    get user_products_url(@user)
    assert_response :success 
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end


  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get login" do
    get users_login_url
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { email: @user.email, name: @user.name } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end
end
