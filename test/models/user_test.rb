require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(name: "Viet Dang Huy", email: "vdang@gustavus.edu", password: "tested_password", password_confirmation: "tested_password")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end

  test "password should be present" do
    @user.password = " "*6
    @user.password_confirmation = " " *6
    @user.valid?
  end
  
  test "password should match password confirmation" do
    @user.password = "hello world!"
    @user.password_confirmation = "hello world!"
    assert @user.valid? 
  end

  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end

  test "name and email are just long enough" do
    @just_long_enough_name = "a" * 99;
    @just_long_enough_email = "a" * 89 + "@gmail.com";
    @user.name = @just_long_enough_name
    @user.email = @just_long_enough_email
    assert @user.valid?
  end

  test "name and email are too long" do
    @long_name = "a"*101;
    @long_email = "a"*91 + "@gmail.com";
    @user.name = @long_name
    @user.email = @long_email
    assert_not @user.valid?
  end

  test "email should be valid" do
    list_of_emails = ["vdang@gustavus.edu","vietdanghuy95@gmail.com","vietdanghuy95@gmail.com"]
    list_of_emails.each do |e|
      @user.email = e
      assert @user.valid?, "#{e.inspect} should be valid"
    end
  end

  test "email should be invalid" do
    list_of_emails = ["vietdanghuy.","vdang@..email.com"]
    list_of_emails.each do |e| 
      @user.email = e 
      assert_not @user.valid?, "#{e.inspect} should be invalid"  
    end
  end

  test "emails should be lower-case before save" do
    actual_email = "VIETdanghuy@gmail.com"
    saved_email = "vietdanghuy@gmail.com"
    @user.email = actual_email
    @user.save
    assert_equal @user.email, saved_email
  end
end
